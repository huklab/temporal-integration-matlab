## Folder structure

* __bin/ (.m)__ - creates figures using data in fits/

* __fits/ (.csv)__ - data created by data analysis [repository](https://bitbucket.org/huklab/temporal-integration-python)

* __plots/ (.pdf)__ - final figures created by bin/